import gym
import gym_Rubiks_Cube
import random
import numpy as np

import neat
import pickle

env = gym.make("RubiksCube-v0")
solved = [5,5,5,5,5,5,5,5,5,
        2,2,2,2,2,2,2,2,2,
        1,1,1,1,1,1,1,1,1,
        0,0,0,0,0,0,0,0,0,
        3,3,3,3,3,3,3,3,3,
        4,4,4,4,4,4,4,4,4]



def mse(y, y_pred):
    return np.mean((np.array(y) - np.array(y_pred))**2)



def eval_genomes(genomes, config):

    for genome_id, genome in genomes:
        obs = env.reset()
        net = neat.nn.FeedForwardNetwork.create(genome, config)
        fitness = 0
        listofmoves = []
        env.setScramble(1, 10, True)
        #print("****************** START ******************")
        for i in range(1,1000):
            
            
            nnout = net.activate(obs)
            obs, rew, done, others = env.step(np.argmax(nnout))
            print(rew, done)
            listofmoves.append(np.argmax(nnout))
            env.render()
            if mse(solved, obs) == 0:
                
                fitness += 1/(i)
                #print(len(listofmoves), env.getlog())

                break
            
            
            #if done: 
                #break
            
        genome.fitness = fitness
            

config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                     neat.DefaultSpeciesSet, neat.DefaultStagnation,
                     'config')

p = neat.Population(config)

p.add_reporter(neat.StdOutReporter(True))
stats = neat.StatisticsReporter()
p.add_reporter(stats)

winner = p.run(eval_genomes)



with open('winner.pkl', 'wb') as output:
    pickle.dump(winner, output, 1)
    
